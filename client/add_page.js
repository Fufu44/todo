  Template.add.helpers({
    
  });
  Template.add.events({
    'submit #add_item': function(event) {
      event.preventDefault();
      if (Meteor.user()) {
        var values = {
          name: event.target.item_name.value,
          type:event.target.item_type.value,
          desc:event.target.item_description.value,
          amount:event.target.item_amount.value,
          user: Meteor.userId()
        };
        Meteor.call('addItem', values);
        alert('Item : ' + event.target.item_name.value + ' Added');
        FlowRouter.go('/home');
      };
     
    }

  });