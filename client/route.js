FlowRouter.route('/', {
  name: 'main page',
  action: function(params) {
    console.log("This is /' page:", params.postId);
    BlazeLayout.render('main_template', {sidebar: 'side_bar', content: 'home'});
  }
});

FlowRouter.route('/home', {
  name: 'Home page',
  action: function(params) {
    console.log('This is home page:', params.postId);
    BlazeLayout.render('main_template', {sidebar: 'side_bar', content: 'home'});
  }
});

FlowRouter.route('/add', {
  name: 'Add Item page',
  action: function(params) {
    console.log('This is add page:', params.postId);
    BlazeLayout.render('main_template', {sidebar: 'side_bar', content: 'add'});
  }
});
FlowRouter.route('/contact', {
  name: 'Contact Us page',
  action: function(params) {
    console.log('This is contact page:', params.postId);
    BlazeLayout.render('main_template', {content: 'contact'});
  }
});

FlowRouter.route('/profile', {
  name: 'Profile page',
  action: function(params) {
    console.log('This is profile page:', params.postId);
    BlazeLayout.render('main_template', {content: 'profile'});
  }
});

FlowRouter.route('/test_temp', {
  name: 'Template test page',
  action: function(params) {
    console.log('This is test page:', params.postId);
    BlazeLayout.render('main_template', {content: 'test_temp'});
  }
});

FlowRouter.route('/test_c3', {
  name: 'Template c3 graphs test page',
  action: function() {
    console.log('This is c3 graphs test page:');
    BlazeLayout.render('main_template', {content: 'test_c3'});
  }
});

FlowRouter.route('/test_map', {
  name: 'Template map test page',
  action: function() {
    console.log('This is map test page:');
    BlazeLayout.render('main_template', {content: 'test_map'});
  }
});