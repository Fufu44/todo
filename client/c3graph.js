AccountLikes = new Mongo.Collection('likes');
Template.test_c3.onCreated(function() {
  Meteor.subscribe('getLikes', {
    onReady: (...args) => { console.log(`subscrition Ready: ${args}`); },
    onError: (...args) => { console.log(`subscription not Ready: ${args}`); }
  });
  
  this.thisTempDict = new ReactiveDict();
  this.thisTempDict.set('likesArr', ['Likes', 1, 2, 3, 4, 5, 6]);
  this.thisTempDict.set('followersArr', ['Followers', 130, 100, 140, 200, 150, 50]);
  this.thisTempDict.set('xAxisArr', ['x', 1, 2, 3, 4, 5, 6, 7, 8, 9]);
});

Template.test_c3.onRendered(function() {
  
  var self = this;
  self.autorun( () => {
    let theReport = AccountLikes.find().fetch(),
        likesOnly = _.pluck(theReport, 'numOfLikes'),
        followersOnly = _.pluck(theReport, 'numOfFollowers');
    self.thisTempDict.set('likesArr', ['Likes', ...likesOnly]);
    self.thisTempDict.set('followersArr', ['Followers', ...followersOnly]);
  });
  
});


Template.test_c3.helpers({
  firstC3Chart: function() {
    return {
      data: {
        labels: false,
        x: 'x',
        columns: [
          Template.instance().thisTempDict.get('xAxisArr'),
          Template.instance().thisTempDict.get('likesArr'),
          Template.instance().thisTempDict.get('followersArr')
        ],
        
        // tooltip: {
        //   grouped: false,
        //   format: {
        //     title: function(d) { return 'Data ' + d; },
        //     value: function(value, ratio, id) {
        //       let format = id === 'Likes' ? d3.format('people liked') : d3.format('people followed');
        //       return format(value);
        //     }
        //   }
        // },
        empty: {
          label: {
            text: 'No Data',
          }
        },
        axes: {
          Followers: 'y2'
        },
        types: {
          Followers: 'line'
        },
        names: {
          Likes: 'Number of Likes',
          Followers: 'Number of Followers'
        },
        
        // hide: ['Likes'],
        groups: [['Followers', 'Likes']],
        type: 'area-step'
      },
      zoom: {
        enabled: true
      },
      transition: {
        duration: 1000
      },
      onmouseover: function() {
        console.log(`working mouse`);
      },
      axis: {
        x: {
          label: {
            text: 'days',
            position: 'outer-middle'
          }
        },
        y: {
          label: {
            text: 'Number of Likes',
            position: 'outer-middle'
          }
        },
        y2: {
          label: {
            text: 'Number of Followers',
            position: 'outer-middle'
          },
          show: true
        }
      }
    };
  }
  
});
Template.test_c3.events({
  'click .changeData': (evt, tmp) => {
    let accountLikes = [],
        accountFollowers = [],
        xAxis = ['x'];
    for ( let i = 0; i < 30; i++) {
      accountLikes.push( Math.random() * 800);
      accountFollowers.push( Math.random() * 1000);
      xAxis.push( String(i + 1) );
    }
    tmp.thisTempDict.set('likesArr', ['Likes', ...accountLikes]);
    tmp.thisTempDict.set('followersArr', ['Followers', ...accountFollowers]);
    tmp.thisTempDict.set('xAxisArr', xAxis);
  },
  'click .addDataButton': (evt, tmp) => {
    let accountLikes = tmp.thisTempDict.get('likesArr'),
        accountFollowers = tmp.thisTempDict.get('followersArr'),
        xAxis = tmp.thisTempDict.get('xAxisArr');
    for ( let i = 0; i < 5; i++) {
      accountLikes.push( Math.random() * 900);
      accountFollowers.push( Math.random() * 1000);
      xAxis.push( xAxis[xAxis.length - 1] + 1 );
    }
    tmp.thisTempDict.set('likesArr', [...accountLikes]);
    tmp.thisTempDict.set('followersArr', [...accountFollowers]);
    tmp.thisTempDict.set('xAxisArr', [...xAxis]);
  },
  'click .enterLikes': (evt, tmp) => {
    let objPrm = {
      likes: Number($('.numlikes').val()),
      followers: Number($('.numfollowers').val())
    };
    Meteor.call('addLike', objPrm);
    console.log(`submit button is working: ${objPrm}`);
    $('.numlikes').val('');
    $('.numfollowers').val('');
  },
  
  'click .dataDB': (evt, tmp) => {
    let tempLikes = _.map(AccountLikes.find({}).fetch(), (obj) => obj.numOfLikes);
    tmp.thisTempDict.set('likesArr', ['Likes', ...tempLikes]);
  }
  
  
});