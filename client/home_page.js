Template.home.onCreated( function() {
    Meteor.subscribe('items');
	Session.set( 'editName', '');
	Session.set( 'editType', '');
	Session.set( 'editDesc', '');
	Session.set( 'editamount', 0);
	Session.set( 'editId', 0);


	//passing data through ReactiveDict
	// this.homeTemplateDict = new ReactiveDict();
	// this.homeTemplateDict.set({
	// 			name: '',
 //        type: '',
 //        desc: '',
 //        amount: 0
	// });

});
Template.home.onRendered(function(){
  this.$('[myAttr = editName]').attr(value, Session.get('editName'));
	this.$('[myAttr = editType]').attr(value, Session.get('editType'));
	this.$('[myAttr = editDesc]').attr(value, Session.get('editDesc'));
	this.$('[myAttr = editAmount]').attr(value, Session.get('editamount'));
});
Template.home.helpers({
    items: function(){
        return Items.find({}); 
    },
    getName:function(){
    	return Session.get('editName');
    	//return Template.instance().homeTemplateDict.get('name');
    },
    getType:function(){
    	return Session.get('editType');
    	//return Template.instance().homeTemplateDict.get('type');
    },
    getDesc:function(){
    	return Session.get('editDesc');
    	//return Template.instance().homeTemplateDict.get('desc');
    },
    getAmount:function(){
    	return Session.get('editAmount');
    	//return Template.instance().homeTemplateDict.get('amount');
    }
});


Template.home.events({

	'click #edit_item_form': function(event){
      event.preventDefault();
      var values = {
        name: event.target.edit_item_name.value,
        type:event.target.edit_item_type.value,
        desc:event.target.edit_item_description.value,
        amount:event.target.edit_item_amount.value
};
			var editId = Session.get('editId');
      Meteor.call('updateItem' ,editId, values);
      console.log('Item : '+ event.target.edit_item_name.value +' Edited and Saved');
      
    }


});





// functions for item_display






Template.item_display.onRendered(function() {
});
Template.item_display.helpers({
});
Template.item_display.events({
		'click #delete_item': function() {
					 Meteor.call('deleteItem', this._id);
		},
		'click #edit_item': function(event,temp){
			 event.preventDefault();
				var values = {
        name: temp.data.name,
        type: temp.data.type,
        desc: temp.data.discription,
        amount: temp.data.no_of_items
				};
				var findId = temp.data._id;
				
				Session.set( 'editName', values.name);
				Session.set( 'editType', values.type);
				Session.set( 'editDesc', values.desc);
				Session.set( 'editamount', values.amount);
				Session.set( 'editId', findId);
				$('.ui.modal').modal('show');
		}
    
});





// functions for edit_item_template Template 





Template.edit_item_template.onCreated( function() {
});
Template.edit_item_template.onRendered(function(){
});
Template.edit_item_template.helpers({
});
Template.edit_item_template.events({
});


