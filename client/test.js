var accountHistory = [
  {likes: 100, followers: 100},
  {likes: 200, followers: 200},
  {likes: 420, followers: 300},
  {likes: 270, followers: 500},
  {likes: 290, followers: 300},
  {likes: 150, followers: 300},
  {likes: 140, followers: 200},
  {likes: 860, followers: 500},
  {likes: 564, followers: 400},
  {likes: 784, followers: 300},
  {likes: 785, followers: 600},
  {likes: 541, followers: 400},
  {likes: 958, followers: 400},
  {likes: 724, followers: 900},
  {likes: 862, followers: 700},
  {likes: 726, followers: 400},
  {likes: 678, followers: 200},
  {likes: 298, followers: 600},
  {likes: 124, followers: 500}
];


Template.test_temp.onRendered(function() {
  
  // console.log('test page onRendered');
});
Template.test_temp.onCreated(function() {
  
  // console.log('test page onCreated');
  this.testTempDict = new ReactiveDict();
  this.testTempDict.set( 'datesArr', [] );
  this.testTempDict.set('likesArr', []);
  this.testTempDict.set('followersArr', []);
});


Template.test_temp.helpers({
  topGenresChart: function() {
    return {
      
      chart: {
        zoomType: 'x',
        borderWidth: 1,
        resetZoomButton: {
          position: {
            x: -10,
            y: 10
          },
          relativeTo: 'chart'
        },
        
        events: {
          click: function(event) {
            var label = this.renderer.label(
                        'x: ' + Highcharts.numberFormat(event.xAxis[0].value, 2) + ', y: ' + Highcharts.numberFormat(event.yAxis[0].value, 2),
                        event.xAxis[0].axis.toPixels(event.xAxis[0].value),
                        event.yAxis[0].axis.toPixels(event.yAxis[0].value)
                    )
                        .attr({
                          fill: Highcharts.getOptions().colors[0],
                          padding: 10,
                          r: 5,
                          zIndex: 8
                        })
                        .css({
                          color: '#FFFFFF'
                        })
                        .add();

            setTimeout(function() {
              label.fadeOut();
            }, 1000);
          }
        }
      },
      
      credits:{
        enabled: false
      },
      
      
      title: {
        text: 'Day2Day follower stats',
        x: -20
      },
      
      xAxis: {
        categories: Template.instance().testTempDict.get('datesArr'),
        labels: {
          autoRotation: [-10, -20, -30, -40, -50, -60, -70, -80, -90]
         
         // staggerLines: 3,
         // step: 1
        }
      },
      
      yAxis: {
        title: {
          text: 'Followers'
        },
        plotLines: [{
          value: 0,
          width: 1,
          color: '#808080'
        }]
      },
      
      tooltip: {
        valueSuffix: 'People'
      },
      
      plotOptions: {
        line:{
          cursor: 'pointer',
          point: {
            events: {
              click: function(event) {
                console.log('You just clicked the graph with Event : ');
              }
            }
          },
        }
      },
      
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
      },
      
      // labels: {
      //   items:[{
      //     html: 'test Label 114,where is it located',
      //     style:{
      //       left: '0',
      //       top: '90px'
      //     }
      //   }],
      //   style: {color: '#7CB5EC'}
      // },
      series: [{
        name: 'Likes',
        data: Template.instance().testTempDict.get('likesArr')
      }, {
        name: 'Followers',
        data: Template.instance().testTempDict.get('followersArr')
      }]
    };
  },
  
  columnChart: function() {
    return {
      
      chart: {
        type: 'column',
        borderWidth: 1
      },
      
      title: {
        text: 'Monthly Average Followers/Likes'
      },
      
      subtitle: {
        text: 'Follow/like Stats'
      },
      
      credits: {
        enabled: false
      },
      
      xAxis: {
        categories: Template.instance().testTempDict.get('datesArr'),
        labels: {
          autoRotation: [-10, -20, -30, -40, -50, -60, -70, -80, -90]
        }
      },
      
      yAxis: {
        min: 0,
        title: {
          text: 'People'
        }
      },
      
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
      },
      
      series: [{
        name: 'Likes',
        data: Template.instance().testTempDict.get('likesArr')
      }, {
        name: 'Followers',
        data: Template.instance().testTempDict.get('followersArr')
      }]
      
      
      
    };
  },
  
  areaChart: function() {
    return {
      chart: {
        type: 'area',
        borderWidth: 1
      },
      
      credits: {
        enabled: false
      },
      
      title: {
        text: 'Followers/Likes Area graph'
      },
      
      subtitle: {
        text: 'Daily Stats'
      },
      
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
      },
      
      xAxis: {
        categories: Template.instance().testTempDict.get('datesArr'),
        labels: {
          autoRotation: [-10, -20, -30, -40, -50, -60, -70, -80, -90]
        }
      },
      
      yAxis: {
        min: 0,
        title: {
          text: 'People'
        }
      },
       
      series: [{
        name: 'Likes',
        data: Template.instance().testTempDict.get('likesArr')
      }, {
        name: 'Follow',
        data: Template.instance().testTempDict.get('followersArr')
      }]
    };
  },
  
  pieChart: function() {
    return {
      chart:{
        type: 'pie'
      },
      
      title: {
        text: 'Monthly Average Followers/Likes'
      },
      
      subtitle: {
        text: 'Follow/like Stats'
      },
      
      credits: {
        enabled: false
      },
      
      plotOptions: {
        pie: {
          allowPointSelect: true,
          borderWidth: 1,
          cursor: 'pointer',
          size: '75%',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          },
          states: {
            hover: {
              brightness: 0.5,
              halo: {
                attributes: {height: 4}
              }
            }
          }
        }
      },
      
      series: [{
        name: 'Followers',
        colorByPoint: true,
        data:  [
          ['Chrome 13', 20.0],
          ['IE 9', 14.0],
          ['Firefox 12', 12.0],
          ['IE 8', 12.0],
          ['Safari', 8.5],
          ['Opera', 6.2],
          ['Something', 5.7],
          ['Else', 5.5],
          ['Others', 5.4]]
      }]
    };
  }
  
  
  
});

function getDates(startDate, stopDate) {
  var dateArray = [];
  var currentDate = startDate;
  while (currentDate <= stopDate) {
    dateArray.push(currentDate);
    currentDate = currentDate.addDays(1);
  }
  return dateArray;
}

Template.test_temp.events({
  'click .button1': function(evt, tmp) {
    let dateArray = getDates(new Date(), (new Date()).addDays(30));
    let arr = [];
    _.each(dateArray, function(i) {
      arr.push( i.getDate() + ' ' + i.getMonthName() );
    });
    tmp.testTempDict.set('datesArr', arr);
   
    
    
    let accountLikesArr = [];
    let accountFollowersArr = _.map(accountHistory, (acc) => {
      accountLikesArr.push(acc.likes);
      return acc.followers;
    });
    
    tmp.testTempDict.set('likesArr', accountLikesArr);
    tmp.testTempDict.set('followersArr', accountFollowersArr);
    
  },
  'click .button2': function(evt, tmp) {
    let arr = [],
        accountLikesArr = [],
        accountFollowersArr = _.map(accountHistory, (acc) => {
          accountLikesArr.push(Math.random() * 1000);
          return acc.followers;
        });
    
    tmp.testTempDict.set('likesArr', accountLikesArr);
    
   // tmp.testTempDict.set('followersArr', accountFollowersArr);
    
  }
});