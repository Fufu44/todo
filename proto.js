Date.prototype.addDays = (days) => {
  let dat = new Date(this.valueOf());
  dat.setDate(dat.getDate() + days);
  dat;
};
Date.prototype.getMonthName = () => {
  var monthArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  return monthArr[this.getMonth()];
};