Items = new Mongo.Collection('items');
AccountLikes = new Mongo.Collection('likes');
Item = Astro.Class({
  name: 'Item',
  collection: Items,
  fields: {
    name: {
      type: 'string',
      default: 'Name Something'
    },
    type: {
      type: 'string',
      default: 'Type of Something'
    },
    discription: {
      type: 'string',
      default: 'No description added'
    },
    no_of_items: {
      type: 'number',
      default: 0
    },
    user_id: {
      type: 'string',
      default: 0
    },
    createdAt:  'date'
  }
});

Meteor.publish('getLikes', () => {
  return AccountLikes.find();
});

Meteor.publish('items', function() {
  console.log('this is fine');
  if ( !!this.user() ) {
    return Items.find({ user_id: Meteor.userId() });
  } else {
    console.log('user not registered');
    return Items.find({});
  }
});

Meteor.publish('user_name', function() {
  if (this.user()) {
    return users.findOne({});
  }
  return this.ready();
});